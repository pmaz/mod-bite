//
//  Player.swift
//  Mod Bite
//
//  Created by Patryk Mazurkiewcz on 26/01/2020.
//  Copyright © 2020 Patryk Mazurkiewcz. All rights reserved.
//

import Foundation
import RealmSwift

class Player: Object {
    enum Property: String {
        case name, level, profession, registrationDate, sex, bestResult
    }
    @objc dynamic var name: String = ""
    @objc dynamic var level: Int = 0
    @objc dynamic var profession: String = ""
    @objc dynamic var registrationDate: Date?
    @objc dynamic var bestResult: Int = 0
}
