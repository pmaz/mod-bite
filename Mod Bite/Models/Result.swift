//
//  Result.swift
//  Mod Bite
//
//  Created by Patryk Mazurkiewcz on 26/01/2020.
//  Copyright © 2020 Patryk Mazurkiewcz. All rights reserved.
//

import Foundation
import RealmSwift

class Result: Object {
    enum Property: String {
        case score, totalTime, profession, date, position
    }
    @objc dynamic var score: Int = 0
    @objc dynamic var totalTime: Int = 0
    @objc dynamic var levelUp: Bool = false
    @objc dynamic var date: Date?
    @objc dynamic var position: Int = 0
}
