//
//  NewGameViewController.swift
//  Mod Bite
//
//  Created by Patryk Mazurkiewcz on 27/01/2020.
//  Copyright © 2020 Patryk Mazurkiewcz. All rights reserved.
//

import UIKit

class NewGameViewController: UIViewController {
    let startNewGameButton = UIButton()
    let displayScoreboardButton = UIButton()
    let displayPlayerInfoButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(red: 1, green: 1, blue: 0.9098, alpha: 1.0)
        setupNewGameButton()
        setupPlayerButton()
        setupScoreboardButton()
        setupNewGameView()
        startNewGameButton.addTarget(self, action: #selector(startNewGame(sender:)), for: .touchUpInside)
        displayScoreboardButton.addTarget(self, action: #selector(displayScoreboard(sender:)), for: .touchUpInside)
        displayPlayerInfoButton.addTarget(self, action: #selector(displayPlayerInfo(sender:)), for: .touchUpInside)
    }
    
    private func setupScoreboardButton() {
        displayScoreboardButton.backgroundColor = UIColor(red: 0.949, green: 0.8275, blue: 0.2157, alpha: 1.0)
        displayScoreboardButton.setImage(UIImage(systemName: "gauge", withConfiguration: UIImage.SymbolConfiguration(pointSize: 25, weight: .heavy)), for: .normal)
        view.addSubview(displayScoreboardButton)
        displayScoreboardButton.setupButtonConstraints(widthEqual: 50, heightEqual: 50, centerYEqual: nil, centerXEqual: nil)
        displayScoreboardButton.setupButtonAnchorConstraints(topEqual: view.topAnchor, topConstant: 100, leadingEqual: view.leadingAnchor, leadingConstant: 20, trailingEqual: nil, trailingConstant: nil, bottomEqual: nil, bottomConstant: nil)
    }
    
    private func setupPlayerButton() {
        displayPlayerInfoButton.backgroundColor = UIColor(red: 0.949, green: 0.8275, blue: 0.2157, alpha: 1.0)
        displayPlayerInfoButton.setImage(UIImage(systemName: "person", withConfiguration: UIImage.SymbolConfiguration(pointSize: 25, weight: .heavy)), for: .normal)
        view.addSubview(displayPlayerInfoButton)
        displayPlayerInfoButton.setupButtonConstraints(widthEqual: 50, heightEqual: 50, centerYEqual: nil, centerXEqual: nil)
        displayPlayerInfoButton.setupButtonAnchorConstraints(topEqual: view.topAnchor, topConstant: 100, leadingEqual: nil, leadingConstant: nil, trailingEqual: view.trailingAnchor, trailingConstant: -20, bottomEqual: nil, bottomConstant: nil)
    }
    
    private func setupNewGameButton() {
        startNewGameButton.backgroundColor = UIColor(red: 117/255, green: 173/255, blue: 112/255, alpha: 1.0)
        startNewGameButton.setTitle("Start", for: .normal)
        startNewGameButton.layer.cornerRadius = 10
        view.addSubview(startNewGameButton)
        startNewGameButton.setupButtonConstraints(widthEqual: 100, heightEqual: 50, centerYEqual: nil, centerXEqual: view.centerXAnchor)
        startNewGameButton.setupButtonAnchorConstraints(topEqual: nil, topConstant: nil, leadingEqual: nil, leadingConstant: nil, trailingEqual: nil, trailingConstant: nil, bottomEqual: view.bottomAnchor, bottomConstant: -260)
    }
    
    private func setupNewGameView() {
        let smallView = UIView()
        smallView.layer.borderWidth = 4
        view.addSubview(smallView)
        smallView.setupViewConstraints(widthEqual: 250, heightEqual: 250, centerYEqual: nil, centerXEqual: view.centerXAnchor)
        smallView.setupViewAnchorConstraints(topEqual: nil, topConstant: nil, leadingEqual: nil, leadingConstant: nil, trailingEqual: nil, trailingConstant: nil, bottomEqual: view.bottomAnchor, bottomConstant: -320)
        smallView.setupBackgroundImage(imageName: "banana")
    }
    
    @objc func startNewGame(sender: UIButton) {
        if let gameViewController = storyboard?.instantiateViewController(identifier: "gameViewController") {
            navigationController?.pushViewController(gameViewController, animated: true)
        }
    }
    
    @objc func displayScoreboard(sender: UIButton) {
        if let scoreboardViewController = storyboard?.instantiateViewController(identifier: "scoreboardViewController") {
            present(scoreboardViewController, animated: true, completion: nil)
        }
    }
    
    @objc func displayPlayerInfo(sender: UIButton) {
        if let playerInfoViewController = storyboard?.instantiateViewController(identifier: "playerInfoViewController") {
            present(playerInfoViewController, animated: true, completion: nil)
        }
    }
}
