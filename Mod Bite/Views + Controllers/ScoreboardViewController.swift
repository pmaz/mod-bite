//
//  ScoreboardViewController.swift
//  Mod Bite
//
//  Created by Patryk Mazurkiewcz on 27/01/2020.
//  Copyright © 2020 Patryk Mazurkiewcz. All rights reserved.
//

import UIKit

class ScoreboardViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .cyan
    }
}
