//
//  GameViewController.swift
//  Mod Bite
//
//  Created by Patryk Mazurkiewcz on 27/01/2020.
//  Copyright © 2020 Patryk Mazurkiewcz. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {
    let startNewGameButton = UIButton()
    let displayBiteButton = UIButton()
    let displaySwallowButton = UIButton()
    let timerLabel = UILabel()
    let scoreLabel = UILabel()
    
    var viewModel: GameViewModel? {
        didSet {
            //updateUI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupGameView()
        setupSwallowButton()
        setupBiteButton()
        setupFinishButton()
        setupScoreLabel()
        setupTimerLabel()
        startNewGameButton.addTarget(self, action: #selector(finishGame(sender:)), for: .touchUpInside)
        viewModel?.controlGame(isStarted: true)
        updateUI()
    }
    
    private func updateUI() {
        if !isViewLoaded {
            return
        }
        
        guard let viewModel = viewModel else {
            return
        }
        
        timerLabel.text = String(viewModel.gameTime)
        scoreLabel.text = String(viewModel.gameScore)
    }
    
    private func setupScoreLabel() {
        timerLabel.backgroundColor = UIColor(red: 0.949, green: 0.8275, blue: 0.2157, alpha: 1.0)
        timerLabel.text = "Score"
        view.addSubview(timerLabel)
        timerLabel.setupLabelConstraints(widthEqual: 100, heightEqual: 30, centerYEqual: nil, centerXEqual: nil)
        timerLabel.setupLabelAnchorConstraints(topEqual: view.topAnchor, topConstant: 100, leadingEqual: view.leadingAnchor, leadingConstant: 20, trailingEqual: nil, trailingConstant: nil, bottomEqual: nil, bottomConstant: nil)
    }
    
    private func setupTimerLabel() {
        scoreLabel.backgroundColor = UIColor(red: 0.949, green: 0.8275, blue: 0.2157, alpha: 1.0)
        scoreLabel.text = "Timer"
        view.addSubview(scoreLabel)
        scoreLabel.setupLabelConstraints(widthEqual: 100, heightEqual: 30, centerYEqual: nil, centerXEqual: nil)
        scoreLabel.setupLabelAnchorConstraints(topEqual: view.topAnchor, topConstant: 100, leadingEqual: nil, leadingConstant: nil, trailingEqual: view.trailingAnchor, trailingConstant: -20, bottomEqual: nil, bottomConstant: nil)
    }
    
    private func setupBiteButton() {
        displayBiteButton.backgroundColor = UIColor(red: 0.949, green: 0.8275, blue: 0.2157, alpha: 1.0)
        displayBiteButton.setTitle("B", for: .normal)
        displayBiteButton.layer.cornerRadius = 10
        view.addSubview(displayBiteButton)
        displayBiteButton.setupButtonConstraints(widthEqual: 130, heightEqual: 130, centerYEqual: nil, centerXEqual: nil)
        displayBiteButton.setupButtonAnchorConstraints(topEqual: nil, topConstant: nil, leadingEqual: view.leadingAnchor, leadingConstant: 30, trailingEqual: nil, trailingConstant: nil, bottomEqual: view.bottomAnchor, bottomConstant: -520)
    }
    
    private func setupSwallowButton() {
        displaySwallowButton.backgroundColor = UIColor(red: 0.949, green: 0.8275, blue: 0.2157, alpha: 1.0)
        displaySwallowButton.setTitle("D", for: .normal)
        displaySwallowButton.layer.cornerRadius = 10
        view.addSubview(displaySwallowButton)
        displaySwallowButton.setupButtonConstraints(widthEqual: 130, heightEqual: 130, centerYEqual: nil, centerXEqual: nil)
        displaySwallowButton.setupButtonAnchorConstraints(topEqual: nil, topConstant: nil, leadingEqual: view.leadingAnchor, leadingConstant: 265, trailingEqual: nil, trailingConstant: nil, bottomEqual: view.bottomAnchor, bottomConstant: -520)
    }
    
    private func setupFinishButton() {
        startNewGameButton.backgroundColor = UIColor(red: 117/255, green: 173/255, blue: 112/255, alpha: 1.0)
        startNewGameButton.setTitle("Finish", for: .normal)
        startNewGameButton.layer.cornerRadius = 10
        view.addSubview(startNewGameButton)
        startNewGameButton.setupButtonConstraints(widthEqual: 100, heightEqual: 50, centerYEqual: nil, centerXEqual: view.centerXAnchor)
        startNewGameButton.setupButtonAnchorConstraints(topEqual: nil, topConstant: nil, leadingEqual: nil, leadingConstant: nil, trailingEqual: nil, trailingConstant: nil, bottomEqual: view.bottomAnchor, bottomConstant: -200)
    }
    
    private func setupGameView() {
        let smallView = UIView()
        smallView.layer.borderWidth = 4
        view.addSubview(smallView)
        smallView.setupViewConstraints(widthEqual: 250, heightEqual: 250, centerYEqual: nil, centerXEqual: view.centerXAnchor)
        smallView.setupViewAnchorConstraints(topEqual: nil, topConstant: nil, leadingEqual: nil, leadingConstant: nil, trailingEqual: nil, trailingConstant: nil, bottomEqual: view.bottomAnchor, bottomConstant: -260)
        smallView.backgroundColor = .lightGray
    }
    
    @objc func finishGame(sender: UIButton) {
        viewModel?.controlGame(isStarted: false)
        if let gameViewController = storyboard?.instantiateViewController(identifier: "resultViewController") {
            navigationController?.pushViewController(gameViewController, animated: true)
        }
    }
}
