//
//  PlayerInfoViewController.swift
//  Mod Bite
//
//  Created by Patryk Mazurkiewcz on 31/01/2020.
//  Copyright © 2020 Patryk Mazurkiewcz. All rights reserved.
//

import Foundation

import UIKit

class PlayerInfoViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .orange
    }
}
