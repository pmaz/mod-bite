//
//  UIView+Image.swift
//  Mod Bite
//
//  Created by Patryk Mazurkiewcz on 30/01/2020.
//  Copyright © 2020 Patryk Mazurkiewcz. All rights reserved.
//

import UIKit

extension UIView {
    func setupBackgroundImage(imageName: String = "banana") {
        let imageView = UIImageView(frame: UIScreen.main.bounds)
        imageView.image = UIImage(named: imageName)
        imageView.contentMode = .scaleToFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(imageView)
        sendSubviewToBack(imageView)
        self.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 0).isActive = true
        self.leadingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: 0).isActive = true
        self.trailingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: 0).isActive = true
        self.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 0).isActive = true
    }
}
