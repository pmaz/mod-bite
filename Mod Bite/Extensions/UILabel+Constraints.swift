//
//  UILabel+Constraints.swift
//  JustCheckIt
//
//  Created by Patryk Mazurkiewcz on 13/01/2020.
//  Copyright © 2020 Patryk Mazurkiewcz. All rights reserved.
//

import UIKit

extension UILabel {
    func setupLabelConstraints(widthEqual: CGFloat?, heightEqual: CGFloat?, centerYEqual: NSLayoutYAxisAnchor?, centerXEqual: NSLayoutXAxisAnchor?) {
        translatesAutoresizingMaskIntoConstraints = false
        if let width = widthEqual {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        if let height = heightEqual {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
        if let centerXEqual = centerXEqual {
            centerXAnchor.constraint(equalTo: centerXEqual).isActive = true
        }
        if let centerYEqual = centerYEqual {
            centerYAnchor.constraint(equalTo: centerYEqual).isActive = true
        }
    }

    func setupLabelAnchorConstraints(topEqual: NSLayoutYAxisAnchor?, topConstant: CGFloat?, leadingEqual: NSLayoutXAxisAnchor?, leadingConstant: CGFloat?, trailingEqual: NSLayoutXAxisAnchor?, trailingConstant: CGFloat?, bottomEqual: NSLayoutYAxisAnchor?, bottomConstant: CGFloat?) {
        if let topE = topEqual, let constant = topConstant {
            topAnchor.constraint(equalTo: topE, constant: constant).isActive = true
        }
        if let leading = leadingEqual, let constant = leadingConstant {
            leadingAnchor.constraint(equalTo: leading, constant: constant).isActive = true
        }
        if let trailing = trailingEqual, let constant = trailingConstant {
            trailingAnchor.constraint(equalTo: trailing, constant: constant).isActive = true
        }
        if let bottom = bottomEqual, let constant = bottomConstant {
            bottomAnchor.constraint(equalTo: bottom, constant: constant).isActive = true
        }
    }

    func setupLabelConstraintsWithMultiplier(widthEqual: NSLayoutDimension?, widthMultiplier: CGFloat?, heightEqual: NSLayoutDimension?, heightMultiplier: CGFloat?) {
        translatesAutoresizingMaskIntoConstraints = false
        if let width = widthEqual, let multiplier = widthMultiplier {
            widthAnchor.constraint(equalTo: width, multiplier: multiplier).isActive = true
        }
        if let height = heightEqual, let multiplier = heightMultiplier {
            heightAnchor.constraint(equalTo: height, multiplier: multiplier).isActive = true
        }
    }
}
