//
//  UIView+ViewConstraints.swift
//  JustCheckIt
//
//  Created by Patryk Mazurkiewcz on 13/01/2020.
//  Copyright © 2020 Patryk Mazurkiewcz. All rights reserved.
//

import UIKit

extension UIView {
    func setupViewAnchorConstraints(topEqual: NSLayoutYAxisAnchor?, topConstant: CGFloat?, leadingEqual: NSLayoutXAxisAnchor?, leadingConstant: CGFloat?, trailingEqual: NSLayoutXAxisAnchor?, trailingConstant: CGFloat?, bottomEqual: NSLayoutYAxisAnchor?, bottomConstant: CGFloat?) {
        translatesAutoresizingMaskIntoConstraints = false
        if let topE = topEqual, let constant = topConstant {
            topAnchor.constraint(equalTo: topE, constant: constant).isActive = true
        }
        if let leading = leadingEqual, let constant = leadingConstant {
            leadingAnchor.constraint(equalTo: leading, constant: constant).isActive = true
        }
        if let trailing = trailingEqual, let constant = trailingConstant {
            trailingAnchor.constraint(equalTo: trailing, constant: constant).isActive = true
        }
        if let bottom = bottomEqual, let constant = bottomConstant {
            bottomAnchor.constraint(equalTo: bottom, constant: constant).isActive = true
        }
    }

    func setupViewConstraints(widthEqual: CGFloat?, heightEqual: CGFloat?, centerYEqual: NSLayoutYAxisAnchor?, centerXEqual: NSLayoutXAxisAnchor?) {
        translatesAutoresizingMaskIntoConstraints = false
        if let width = widthEqual {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        if let height = heightEqual {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
        if let centerXEqual = centerXEqual {
            centerXAnchor.constraint(equalTo: centerXEqual).isActive = true
        }
        if let centerYEqual = centerYEqual {
            centerYAnchor.constraint(equalTo: centerYEqual).isActive = true
        }
    }
}
