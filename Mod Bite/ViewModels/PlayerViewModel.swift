//
//  PlayerViewModel.swift
//  Mod Bite
//
//  Created by Patryk Mazurkiewcz on 01/02/2020.
//  Copyright © 2020 Patryk Mazurkiewcz. All rights reserved.
//

import Foundation

struct PlayerViewModel {
    let name: String
    let level: Int
    let profession: String
    let registrationDate: Date
    let bestResult: Int
    
    init(player: Player) {
        self.name = player.name
        self.level = player.level
        self.profession = player.profession
        self.registrationDate = player.registrationDate ?? Date()
        self.bestResult = player.bestResult
    }
}
