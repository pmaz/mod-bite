//
//  GameViewModel.swift
//  Mod Bite
//
//  Created by Patryk Mazurkiewcz on 01/02/2020.
//  Copyright © 2020 Patryk Mazurkiewcz. All rights reserved.
//

import Foundation

protocol GameProtocol {
    var gameTime: Int { get }
    var gameScore: Int { get }
    var speed: Int { get }
    var bites: Int { get }
    var position: Int { get }
    var isStarted: Bool { get }
    
    func controlGame(isStarted:Bool)
}

class GameViewModel: GameProtocol {

    var gameTime: Int
    var gameScore: Int
    var speed: Int
    var bites: Int
    var position: Int
    var isStarted: Bool
    
    init(game: Game) {
        self.isStarted = game.isStarted
        self.gameTime = game.time
        self.gameScore = game.score
        self.speed = game.speed
        self.bites = game.bites
        self.position = game.position
    }

    func controlGame(isStarted: Bool) {
        if isStarted == true {
            startTimer()
        }
    }
    
    private var timer: Timer?
    private func startTimer() {
        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
            guard self.isStarted == true else {
                timer.invalidate()
                return
            }
            self.gameTime += 1
        }
    }
    
    private func timeString(time: TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        switch time {
        case 0..<3600:
            return String(format: "%02d:%02d", minutes, seconds)
        default:
            return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        }
    }
    
    private func stopTimer() {
        timer?.invalidate()
        print("timer inavlidated")
    }
    
}
