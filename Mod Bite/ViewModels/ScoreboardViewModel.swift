//
//  ScoreboardViewModel.swift
//  Mod Bite
//
//  Created by Patryk Mazurkiewcz on 01/02/2020.
//  Copyright © 2020 Patryk Mazurkiewcz. All rights reserved.
//

import Foundation

struct ScoreboardViewModel {
    let score: Int
    let totalTime: Int
    let levelUp: Bool
    let date: Date
    let position: Int
    
    init(result: Result) {
        self.score = result.score
        self.totalTime = result.totalTime
        self.levelUp = result.levelUp
        self.date = result.date ?? Date()
        self.position = result.position
    }
}
